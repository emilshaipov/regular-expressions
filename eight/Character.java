package ru.ser.eight;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Character {

    public static void main(String[] args) {

        int counter = 0;
        String string = "abcd1234";
        Pattern pattern = Pattern.compile("\\p{Digit}");
        Matcher matcher = pattern.matcher(string);


        while (matcher.find()) {
            counter++;
            System.out.println("Match found '" + string.substring(matcher.start(),
                    matcher.end()) + "'. Starting at index " + matcher.start()
                    + " and ending at index " + matcher.end());
        }

        System.out.println("Matcher found: " + counter);

    }

}
